package main

import "log"

type rectangle struct {
	length  float64
	breadth float64
	width   float64
}

type circle struct {
	radius float64
}

type triangle struct {
	base   float64
	height float64
}

type dimensions struct {
	rectangle rectangle
	cirle     circle
	triangle  triangle
}
type Shapes interface {
	tankArea() float64
	tankPerimeter() float64
	tankVolume() float64
	triangleArea() float64
	circlePerimeter() float64
	circleArea() float64
}

func (d dimensions) tankArea() float64 {
	area := d.rectangle.length * d.rectangle.breadth
	return area
}
func (d dimensions) tankPerimeter() float64 {
	perimeter := 2 * (d.rectangle.length + d.rectangle.breadth)
	return perimeter
}

func (d dimensions) tankVolume() float64 {
	volume := d.rectangle.length * d.rectangle.breadth * d.rectangle.width
	return volume
}

func (d dimensions) triangleArea() float64 {
	area := d.triangle.base * d.triangle.height * 0.5
	return area
}

func (d dimensions) circlePerimeter() float64 {
	perimeter := 2 * 3.14 * d.cirle.radius
	return perimeter
}

func (d dimensions) circleArea() float64 {
	area := 3.14 * d.cirle.radius * d.cirle.radius
	return area
}

func main() {

	//create variable of interface; interface can access the types that method is implementing
	var s Shapes = dimensions{rectangle: rectangle{1, 3, 5},
		cirle:    circle{2.5},
		triangle: triangle{2.5, 4},
	}
	

	// Tank details
	log.Println("Area of tank:", s.tankArea())
	log.Println("Perimeter of the tank:", s.tankPerimeter())
	log.Println("Volume of the tank:", s.tankVolume())

	// Triangle details
	log.Printf("area of triangle: %f", s.triangleArea())

	// circle deatils
	log.Printf("area of circle: %f", s.circleArea())
	log.Printf("perimeter of circle: %f", s.circlePerimeter())

}
